# ECA Message
Plug & Play solution to make it possible to create <a href="https://www.drupal.org/project/message">message</a> entities with the <a href="https://www.drupal.org/project/eca">ECA</a> module.

### Features
Temporary solution till https://www.drupal.org/project/eca/issues/3375899 is resolved.

### Additional Requirements
* <a href="https://www.drupal.org/project/eca">ECA: Event - Condition - Action
* <a href="https://www.drupal.org/project/message">Message</a> 8.x-1.x-dev or with the patch from https://www.drupal.org/project/message/issues/3331242